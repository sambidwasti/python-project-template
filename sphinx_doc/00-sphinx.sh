#!/bin/zsh
sphinx-apidoc -fFM -o . ../project
sphinx-apidoc -fM -o . ../project
cp -r _temp_files/* .
mv ./index.txt ./index.rst
make clean
make html
