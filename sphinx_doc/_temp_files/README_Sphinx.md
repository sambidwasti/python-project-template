# Python Project
This is a repository of the Python Project.

# Temp Edits/Renames/Quick notes.
This project is currently setup for a project name 'project'. The folder is project,
the sphinx documentation, gitlab ci, etc is customized for that name.

Here are some of the places this needs to be updated:

- Sphinx/_temp/config 
    - need to fix the path to the code.
    - update the project name, etc.
    - update the readme (markdown format)
-change name of the code file. 
# Folder/Files

gitlab-ci to deploy/test/etc.

requirements.txt
  - This has all the requirements needed for the project. This might not be a complete set. 
  - Additionally, can create another one for the gitlab-ci.

## sphinx-doc
- This folder contains the files related to [sphinx](https://www.sphinx-doc.org/en/master/)
- It also contains few executable files that has the commands for sphinx.
- The _temp folder has some config file and readme. 
- Ideally, this is setup for the gitlab-ci editor to generate a gitlab page at deploy. 

## project
- This is the main project folder.
- If you want to add more modules / collection of scripts / or submodules, you need
    - an __init__.py file in the folder.
    - add the path in the sphinx config file.


## Running the app  ##

```bash
python app.py
python helloworld.py
```
## Installation ##
Currently, we do not have a setup. We are just running the repo. Ideally (MAYBE), we would install via the following
>**TODO TASK**: Create a setup file
```bash
pip3 install . 
```
# Workflow

- Master is a protective main branch.
- Dev is a development branch. 
    - changes are to be added to the dev and after sufficient changes, it should be
    pushed to the main with a tag and release.
      
Refer to [this document](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) for additional information about the workflow.

## Documentation Style

### Code
Of course abide by [PEP 8](https://www.python.org/dev/peps/pep-0008/), with the exception
of maximum line lengths. We are using 90 characters as our maximum line length. For bonus
points, it is recommended that code is formatted with
[python black](https://pypi.org/project/black/).


### Docstrings
We are attempting to adhere to the
[numpy docstring guide](https://numpydoc.readthedocs.io/en/latest/format.html) or 
[google docstring guide](https://google.github.io/styleguide/pyguide.html)

The differences can be seen here:
[google doc string](https://bwanamarko.alwaysdata.net/napoleon/format_exception.html)


## Sphinx Documentation


### Steps and Description      

**Running basic scripts**    

## Sphinx stuff
To initiate sphinx, just run 00-sphinx.sh (it's an executable script)

You might have to change the config to point to the project. 
And install few requirements if any.

The index calls in modules and README_Sphinx.md which a readme you are going to see in the 
gitlab pages and not the main readme. 

After that, to see the documentation, go to sphinx-doc/_build/html/index.html

In the sphinx-doc folder, to refresh/reset, just delete everything except the 01-03 executable 
script and the _temp_files folder.        

---
>**NOTE/Warning: You can write warnings here.**

