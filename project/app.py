import dash_devices
from dash_devices.dependencies import Input, Output
import dash_html_components as html
import dash_core_components as dcc

app = dash_devices.Dash(__name__)
app.config.suppress_callback_exceptions = True


if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=5000)